package main

import (
	"bitbucket.org/grubersbucket/crawler/crawl"
	"fmt"
	"net/url"
	"os"
	"time"
)

func main() {
	if len(os.Args) != 2 {
		printHelp()
		os.Exit(1)
	}
	if os.Getenv("CRAWL_PROFILING") == "true" {
		Profile(doCrawling)
	} else {
		doCrawling()
	}
}

func doCrawling() {
	domain := os.Args[1]
	stream := crawl.NewChannelStream()
	storage, err := crawl.NewFileStorage(dirName(domain))
	if err != nil {
		panic(err)
	}
	err = crawl.Crawl(domain, crawl.GoWorker, storage, stream)
	if err != nil {
		fmt.Fprintf(os.Stdout, "error: %s\n", err.Error())
		os.Exit(2)
	}
}
func dirName(domain string) string {
	url, _ := url.Parse(domain)
	return fmt.Sprintf("%s_%s", url.Hostname(), time.Now().Format(time.RFC3339))
}

func printHelp() {
	fmt.Println("Usage: crawler https://mypage.com")
}
