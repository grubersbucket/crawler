# A simple http/html crawler which follows only links from the given domain

## Usage

    crawl https://mydomain.sth

All through embedded links retrieved pages (and links of links) from the given
domain (external links are removed) are stored in a newly generated subdirectory
as file. This file contains all internal links found on that url as new lines.
The filename is the bas64 encoded URL. For decoding see _scripts_.

The numbers shown on _stdout_ are the amount of work-units (URLs) in the queue 
+ currently in process by workers.

## Design Decissions

### Output

Output is in files but can be easily adjusted to be in NoSQL, Object, or HDFS Storage
though usage of an interface.

### Processing Units

Processing is done in coroutines since that is the easiest way to test and
should be enough for scanning a medium sized single domain. Communication
is therefore done by go channels.

Through the usage of interfaces processing units and communication could be
changed to processes and streaming frameworks (like kafka, AMQP, etc.).

### Memory Requirements

It's is an IO intensive task. Through parallism it can be memory and CPU intensive
(96 go-routines). It might be necessary to adapt the amount of go-routines to match
the hardware, but it also needs to considered that too many go-routines can lead to
internal IO timeouts.

For detailed profiling and tracing of the application:

    CRAWL_PROFILING=true crawl https://mydomain.sth

which creates a profiles directory.

### Linked URLs

When they can not be retrieved they will not be stored as keys.

### Quality

Test coverage:
- go test -coverprofile=coverage.out && go tool cover -func=coverage.out && go tool cover -html=coverage.out

Race conditions:
- go test -race .
- go build -race


