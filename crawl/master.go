package crawl

import (
	"net/url"
	"strings"
	"sync"
	"sync/atomic"
)

// Filter stores all seen pages and tells if a links was
// already seen.
type Filter struct {
	sync.Mutex
	// urls contains all url which went into the channel for the
	// workers -> if set to true it was processed if false it is
	// not yet processed
	urls sync.Map
}

// Store stores an URL as known by the filter and
// return true if it was known before and must be
// therefore filtered.
func (f *Filter) Store(url string) bool {
	f.Lock()
	defer f.Unlock()
	// only add not yet seen pages
	if _, exists := f.urls.Load(url); exists {
		return true // this URL was already sent to worker
	}
	f.urls.Store(url, false)
	return false
}

// StartMaster starts a routine which receives newly found URLs from the
// workers and checks if the URL is already sent to the stream. If not it
// sends it to the streamer to which workers are connected.
// Master sends only valid domains to workers.
// Outputs are: inputput channels from client to master, waitgroup for
// all created go-routines, and counter (which when 0 means that all
// new links passed the master)
func StartMaster(domain *url.URL, seeds []string, streamer Streamer) ([]Streamer, *sync.WaitGroup, *int64) {
	var sitesToProcess int64

	workerStreams := make([]Streamer, streamer.MaxWorkers())

	wgWorkers := sync.WaitGroup{}
	wgWorkers.Add(len(workerStreams))

	for i := range workerStreams {
		workerStreams[i] = NewChannelStream() // TODO NewMethod must come from main
	}

	var filter Filter

	filter.Store(domain.String())

	if len(seeds) > CHANNEL_CAPACITY {
		panic("too many seed pages to fit in channel")
	}

	for k := range seeds {
		if filter.Store(seeds[k]) {
			continue
		}
		atomic.AddInt64(&sitesToProcess, 1)
		streamer.Send(seeds[k])
	}

	// receive new urls from the workers
	for i := range workerStreams {
		stream := i
		go func() {
			for {
				url, ok := workerStreams[stream].Receive()
				if !ok {
					break
				}
				// check for events
				if strings.HasPrefix(url, "%%processed%%") {
					atomic.AddInt64(&sitesToProcess, -1)
					continue
				}

				// check result
				//if !HasValidDomain(domain, url) {
				//	continue
				//}

				// check filter
				if filter.Store(url) {
					// exists already
					continue
				}

				atomic.AddInt64(&sitesToProcess, 1)
				streamer.Send(url)
			}
			wgWorkers.Done()
		}()
	}

	return workerStreams, &wgWorkers, &sitesToProcess
}
