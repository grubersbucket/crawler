package crawl

import (
	"fmt"
	"log"
	"sync/atomic"
	"time"
)

// Crawl checks a given URL for links to pages with the same domain and
// stores the found links in the given storage. The crawling is performed
// by the given workers, which communicate with the master process by
// using the given streamer interface.
func Crawl(domain string, worker Worker, store Storer, stream Streamer) error {
	base, err := CheckDomain(domain)
	if err != nil {
		return err
	}
	urls, err := DownloadPageAndParseForURLs(domain)
	if err != nil {
		return fmt.Errorf("when scanning the seed page: %s", err.Error())
	}
	if len(urls) == 0 {
		return nil
	}

	filteredURLs := FilterURLs(*base, NormalizeURLs(*base, urls))
	store.Save(domain, filteredURLs)
	if len(filteredURLs) == 0 {
		return nil
	}

	workerValidators, workerFinished, sitesToProcess := StartMaster(base, filteredURLs, stream)
	StartStreamWorkers(worker, *base, stream, workerValidators, store)

	log.Println("start processing")
	Supervise(sitesToProcess)
	WhenFinishedCloseChannel(stream, sitesToProcess)

	log.Println("waiting for workers")
	workerFinished.Wait()

	return nil
}

// Supervise prints out interesting information during runtime out to stdout.
func Supervise(sitesToProcess *int64) {
	go func() {
		for {
			<-time.Tick(time.Millisecond * 50)
			fmt.Printf("queue size: %d\n", atomic.AddInt64(sitesToProcess, 0))
		}
	}()
}

// WhenFinishedCloseChannel waits until all URLs are processed and then
// closes the channel which is used by the master to send URLs to the
// workers. The workers are then shutting down, which let the master go
// down as well (when all workers are disconnected).
func WhenFinishedCloseChannel(stream Streamer, sitesToProcess *int64) {
	ticker := time.NewTicker(time.Millisecond * 50)
	defer ticker.Stop()
	for range ticker.C {
		if atomic.AddInt64(sitesToProcess, 0) == 0 {
			stream.Close()
			break
		}
	}
}
