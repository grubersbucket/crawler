package crawl_test

import (
	. "bitbucket.org/grubersbucket/crawler/crawl"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"encoding/base64"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

var _ = Describe("Storer", func() {

	var url = "https://www.gridengine.eu"

	createTempDir := func() (string, string) {
		parentDir, err := ioutil.TempDir("", "filestoragetesttempdir")
		if err != nil {
			Fail(err.Error())
		}
		return parentDir, filepath.Join(parentDir, "storage")
	}

	Describe("FileStorage", func() {

		Context("Happy path", func() {

			It("should write a file to filesystem", func() {
				parentDir, dir := createTempDir()
				defer os.RemoveAll(parentDir)

				fs, err := NewFileStorage(dir)
				Ω(err).Should(BeNil())
				Ω(fs).ShouldNot(BeNil())
				err = fs.Save(url, []string{"link1", "link2"})
				Ω(err).Should(BeNil())

				content, err := ioutil.ReadFile(filepath.Join(dir, base64.StdEncoding.EncodeToString([]byte(url))))
				Ω(err).Should(BeNil())
				Ω(string(content)).Should(ContainSubstring("link1\n"))
				Ω(string(content)).Should(ContainSubstring("link2\n"))
			})

		})

		Context("Error cases", func() {

			It("should fail when directory already exists", func() {
				parentDir, _ := createTempDir()
				defer os.RemoveAll(parentDir)

				fs, err := NewFileStorage(parentDir)
				Ω(err).ShouldNot(BeNil())
				Ω(fs).Should(BeNil())

			})

			It("should fail when URL is too long for a filename", func() {
				parentDir, dir := createTempDir()
				defer os.RemoveAll(parentDir)

				fs, err := NewFileStorage(dir)
				Ω(err).Should(BeNil())
				Ω(fs).ShouldNot(BeNil())

				longString := strings.Repeat("12345678", 128*8)

				fmt.Println("before")
				err = fs.Save(longString, []string{url})
				Ω(err).ShouldNot(BeNil())
			})

		})
	})

	Describe("MemStorage", func() {

		Context("should work", func() {

			It("should return a value put into storage", func() {
				s := NewMemStorage()
				err := s.Save("url", []string{"links"})
				Ω(err).Should(BeNil())
				links, ok := s.Read("url")
				Ω(ok).Should(BeTrue())
				Ω(links[0]).Should(Equal("links"))
			})

			It("should not return a value when the value is not put into storage", func() {
				s := NewMemStorage()
				err := s.Save("url", []string{"links"})
				Ω(err).Should(BeNil())
				links, ok := s.Read("doesnotexist")
				Ω(ok).Should(BeFalse())
				Ω(links).Should(BeNil())
			})

			It("should create a summary of stored pages", func() {
				s := NewMemStorage()
				err := s.Save("url", []string{"links", "links2", "links3"})
				Ω(err).Should(BeNil())
				err = s.Save("url2", []string{"links", "links2", "links3"})
				Ω(err).Should(BeNil())
				Ω(s.Summary()).Should(BeNumerically("==", 2))
			})

		})

	})

})
