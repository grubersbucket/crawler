package crawl

import (
	"fmt"
	"golang.org/x/net/html"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"
)

// HasValidDomain parses an URL and checks if it has
// a valid domain.
func HasValidDomain(domain *url.URL, iurl string) bool {
	u, err := url.ParseRequestURI(iurl)
	if err != nil {
		return false
	}
	// host or host:port
	if domain.Host == u.Host {
		return true
	}
	return false
}

// ExtractLinks returns all unique links found in the given html page.
func ExtractLinks(body io.Reader) map[string]interface{} {
	urls := make(map[string]interface{}, 4096)
	tokenizer := html.NewTokenizer(body)
	// search for all links
	for {
		token := tokenizer.Next()
		if token == html.ErrorToken {
			break
		}
		t := tokenizer.Token()
		if token == html.StartTagToken && t.Data == "a" {
			for _, a := range t.Attr {
				if a.Key == "href" {
					urls[a.Val] = nil
					break
				}
			}
		}
	}
	return urls
}

// RelativeToAbsoluteURL converts a given link to an absolute URL
// using a given base.
func RelativeToAbsoluteURL(base *url.URL, link string) (*url.URL, error) {
	ref, err := url.Parse(link)
	if err != nil {
		return nil, err
	}
	if ref.Host != "" && base.Host != ref.Host {
		return nil, fmt.Errorf("link does not point to the same host")
	}
	newUrl := base.ResolveReference(ref)
	return newUrl, nil
}

// NormalizeURLs converts all relative URL references to absolute
// URLs and filters the URLs for http and https URLs and removes
// # fragments out of the URLs.
func NormalizeURLs(domain url.URL, imputUrls map[string]interface{}) map[string]interface{} {
	outputURLs := make(map[string]interface{})

	// filter
	for next := range imputUrls {
		// filter all URLs which can't be parsed / only continue with absolute urls
		absolute, err := RelativeToAbsoluteURL(&domain, next)
		if err != nil {
			continue
		}
		// only http and https are allowed
		if absolute.Scheme != "http" && absolute.Scheme != "https" {
			continue
		}
		// remove # fragments in url
		// like: https://www.url.com/email-protection#3c031a4f495e56595f48017f54595f571c5349481c48
		split := strings.Split(absolute.String(), "#")

		newUrl, err := url.Parse(split[0])
		if err != nil {
			fmt.Printf("error parsing: %s\n", err)
		}
		// skip root
		if newUrl.String() == domain.String() {
			continue
		}
		outputURLs[newUrl.String()] = nil
	}
	return outputURLs
}

/*
   Ran 100 samples:
   Download:
     Fastest Time: 0.091s
     Slowest Time: 0.154s
     Average Time: 0.111s ± 0.016s

func OldDownload(url string) (io.ReadCloser, error) {
	client := http.Client{
		Timeout: time.Second * 30,
	}
	resp, err := client.Get(url)
	if err != nil {
		return nil, fmt.Errorf("could not retrieve content from %s: %s", url, err.Error())
	}
	return resp.Body, nil
}

*/

/*
   Ran 100 samples:
   Download:
     Fastest Time: 0.095s
     Slowest Time: 0.321s
     Average Time: 0.125s ± 0.036s

    MaxIdleConns:        2,
	MaxIdleConnsPerHost: 2,

 	Ran 100 samples:
        Download:
          Fastest Time: 0.084s
          Slowest Time: 1.201s
          Average Time: 0.127s ± 0.112s

    MaxIdleConns:        128,
	MaxIdleConnsPerHost: 128,

*/

var client *http.Client

// Download fetches the content of given URL and returns it.
func Download(url string) (io.ReadCloser, error) {
	if client == nil {
		transport := &http.Transport{
			MaxIdleConns:          12,
			MaxIdleConnsPerHost:   12,
			ResponseHeaderTimeout: time.Second * 60,
			TLSHandshakeTimeout:   time.Second * 60,
			ExpectContinueTimeout: time.Second * 60,
		}
		client = &http.Client{Transport: transport}
	}
	resp, err := client.Get(url)
	if err != nil {
		return nil, fmt.Errorf("could not retrieve content from %s: %s", url, err.Error())
	}
	return resp.Body, nil
}

// DownloadPageAndParseForURLs fetches the content of a given URL and
// returns all found links.
func DownloadPageAndParseForURLs(url string) (map[string]interface{}, error) {
	/* IO vs. CPU
	2018/07/14 14:22:14 IO time: 4m49.374559855s
	2018/07/14 14:22:14 CPU time: 9.208698619s
	with 128 go routines
	*/
	// IO intensive
	body, err := Download(url)
	if err != nil {
		return nil, err
	}
	defer body.Close()
	// CPU intensive
	return ExtractLinks(body), nil
}

// FilterURLs returns all URLs with valid domains.
func FilterURLs(domain url.URL, urls map[string]interface{}) []string {
	result := make([]string, 0, len(urls))
	for key := range urls {
		// domain could be parsed once
		if !HasValidDomain(&domain, key) {
			continue
		}
		// TODO: add more checks here if needed
		result = append(result, key)
	}
	// check if URL exists in store
	return result
}

// CheckDomain parses a given URL and returns it.
func CheckDomain(domain string) (*url.URL, error) {
	base, err := url.Parse(domain)
	if err != nil {
		return nil, fmt.Errorf("invalid domain %s: %s", domain, err.Error())
	}
	return base, nil
}
