package crawl

import (
	"fmt"
	"net/url"
	"os"
)

// Worker is an interface for a processor which takes an
// URL from the first stream, analyzes that URL for links
// to the given domain, and sends the links down to the
// second stream.
type Worker func(int, url.URL, Streamer, Streamer, Storer)

// GoWorker implements Worker as coroutine.
func GoWorker(id int, domain url.URL, inputStream, outputStream Streamer, store Storer) {
	go func() {
		for {
			url, more := inputStream.Receive()
			if !more {
				outputStream.Close()
				return
			}
			ProcessUrl(domain, outputStream, store, url)
		}
	}()
}

// ProcessUrl downloads the given url, searches links, normalizes links,
// filters links, and sends the discovered urls from the given domain
// down to the stream. After finished it sends a finished event in the
// stream.
func ProcessUrl(domain url.URL, stream Streamer, store Storer, url string) {
	urls, err := DownloadPageAndParseForURLs(url)
	if err != nil {
		fmt.Fprintf(os.Stderr, "linked url %s can not be retrieved - do not store it\n", url)
		stream.Send("%%processed%%")
		return
	}
	filteredURLs := FilterURLs(domain, NormalizeURLs(domain, urls))
	store.Save(url, filteredURLs)

	for url := range filteredURLs {
		stream.Send(filteredURLs[url])
	}

	stream.Send("%%processed%%")
}

// StartStreamWorkers strts all workers which receivs urls from from the given
// stream. The output of the workers is send to the worker streams individually
// and saved in the given storage.
func StartStreamWorkers(worker Worker, domain url.URL, stream Streamer, workerStream []Streamer, store Storer) {
	for i := 0; i < stream.MaxWorkers(); i++ {
		worker(i, domain, stream, workerStream[i], store)
	}
}
