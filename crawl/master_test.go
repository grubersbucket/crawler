package crawl_test

import (
	. "bitbucket.org/grubersbucket/crawler/crawl"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"net/url"
	"strconv"
	"time"
)

var _ = Describe("Master", func() {

	Context("Filter", func() {

		It("should find already pushed urls", func() {
			var filter Filter
			Ω(filter.Store("new url")).Should(BeFalse())
			Ω(filter.Store("new url")).Should(BeTrue())
			Ω(filter.Store("really a new url")).Should(BeFalse())
		})

	})

	Context("StartMaster", func() {

		It("should panic when channel capacity is overexceeded", func() {
			streamer := NewChannelStream()
			seeds := make([]string, 1024*1024)
			u, _ := url.Parse("https://www.fireball.com")

			Ω(func() {
				StartMaster(u, seeds, streamer)
			}).Should(Panic())
		})

		It("should run the master", func() {
			streamer := NewChannelStream()
			seeds := make([]string, 25)
			for i := 0; i < 24; i++ {
				seeds[i] = strconv.Itoa(i)
			}
			// same working item must be filtered = 24
			seeds[24] = strconv.Itoa(0)

			u, _ := url.Parse("https://www.lycos.com")

			channels, waitgroup, toProc := StartMaster(u, seeds, streamer)

			Ω(len(channels)).Should(BeNumerically("==", streamer.MaxWorkers()))
			Ω(int(*toProc)).Should(BeNumerically("==", 24))

			// client: receive work
			for i := 0; i < 24; i++ {
				val, ok := streamer.Receive()
				Ω(val).Should(Equal(strconv.Itoa(i)))
				Ω(ok).Should(BeTrue())
			}

			// client: sending that work was processed
			for i := 0; i < 24; i++ {
				channels[0].Send("%%processed%%")
			}

			<-time.Tick(time.Millisecond * 50)

			Ω(int(*toProc)).Should(BeNumerically("==", 0))

			channels[0].Send("25")
			<-time.Tick(time.Millisecond * 100)

			Ω(int(*toProc)).Should(BeNumerically("==", 1))

			val, ok := streamer.Receive()
			Ω(val).Should(Equal("25"))
			Ω(ok).Should(BeTrue())

			// stop master by closing all backchannels to master
			for i := 0; i < streamer.MaxWorkers(); i++ {
				channels[i].Close()
			}
			waitgroup.Wait()

		})

	})

})
