package crawl

import (
	"encoding/base64"
	"fmt"
	"os"
	"path/filepath"
	"sync"
)

// Storer is an interface which provides functionalities for
// saving the URL of an page and the links the page has.
type Storer interface {
	Save(string, []string) error
	// Read(string) []string
}

// FileStorage implements Storer and stores the URL and the links
// in single files in a directory which does not have to exist.
// For each website one file is generated.
// The file name (base64 encoded) is the website and the
// content (each link a new line) are the links.
type FileStorage struct {
	dir string
}

// NewFileStorage creates a new directory with the given name
// and returns FileStorage.
func NewFileStorage(dir string) (*FileStorage, error) {
	if err := os.Mkdir(dir, 0700); err != nil {
		return nil, fmt.Errorf("can not create %s directory: %s", dir, err)
	}
	return &FileStorage{dir: dir}, nil
}

// Save creates a new file (the url but base64 encoded) and stores
// all links in new lines in the file.
func (fs *FileStorage) Save(url string, links []string) error {
	// https://www.ietf.org/rfc/rfc4648.txt for encoding since
	// base encoding yields to filesystem issues
	file, err := os.Create(filepath.Join(fs.dir, base64.URLEncoding.EncodeToString([]byte(url))))
	if err != nil {
		fmt.Fprintf(os.Stderr, "can not store links for %s: %s\n", url, err.Error())
		return err
	}
	for i := range links {
		file.WriteString(links[i] + "\n")
	}
	err = file.Close()
	if err != nil {
		fmt.Fprintf(os.Stderr, "can not close links file for %s: %s\n", url, err.Error())
		return err
	}
	return nil
}

// MemStorage stores pages in main memory.
type MemStorage struct {
	m sync.Map
}

// NewMemStorage creates an in memory storage which implements the Storer interface.
func NewMemStorage() *MemStorage {
	return &MemStorage{}
}

// Save stores the URL and links in memory.
func (ms *MemStorage) Save(url string, links []string) error {
	ms.m.Store(url, links)
	return nil
}

// Read searches the store URL and returns the links if found.
func (ms *MemStorage) Read(url string) ([]string, bool) {
	links, ok := ms.m.Load(url)
	if !ok {
		return nil, ok
	}
	return links.([]string), ok
}

// Summary prints out all found links and the amount.
func (ms *MemStorage) Summary() int {
	amount := 0
	ms.m.Range(func(key, value interface{}) bool {
		amount++
		fmt.Println(key.(string))
		return true
	})
	fmt.Printf("Amount of pages: %d\n", amount)
	return amount
}
