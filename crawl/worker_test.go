package crawl_test

import (
	. "bitbucket.org/grubersbucket/crawler/crawl"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

var pageWithThreeLinks = `<html><body><a href="http://www.domain.com/link1">link1</a><a href="https://www.domain.com/link2">link2</a><a href="/build">link3</a></body></html>`

var links = []string{"http://www.domain.com/link1", "https://www.domain.com/link2", "http://www.domain.com/build"}

func ValidLink(link string, links []string) bool {
	for i := range links {
		if links[i] == link {
			return true
		}
	}
	fmt.Println(links)
	return false
}

var _ = Describe("Worker", func() {

	Context("URL Processing", func() {

		It("should download a page and send all relevant links upstream", func() {
			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				fmt.Fprintf(w, "%s", pageWithThreeLinks)
			}))
			defer ts.Close()

			stream := NewChannelStream()
			store := NewMemStorage()

			domain, _ := url.Parse("http://www.domain.com")
			ProcessUrl(*domain, stream, store, ts.URL)

			link, ok := stream.Receive()
			Ω(ok).Should(BeTrue())
			Ω(ValidLink(link, links)).Should(BeTrue())

			link, ok = stream.Receive()
			Ω(ok).Should(BeTrue())
			Ω(ValidLink(link, links)).Should(BeTrue())

			link, ok = stream.Receive()
			Ω(ok).Should(BeTrue())
			Ω(ValidLink(link, links)).Should(BeTrue())

			processed, ok := stream.Receive()
			Ω(ok).Should(BeTrue())
			Ω(processed).Should(Equal("%%processed%%"))

			stream.Close()
			link, ok = stream.Receive()
			Ω(ok).Should(BeFalse())
			Ω(link).Should(Equal(""))
			fmt.Println("links")
			links, found := store.Read(ts.URL)
			Ω(found).Should(BeTrue())
			Ω(len(links)).Should(BeNumerically("==", 3))
		})

		It("should not process URL when page does not exist", func() {
			domain, err := url.Parse("http:/wwwdomaincom123doesnotexit")
			Ω(err).Should(BeNil())
			stream := NewChannelStream()
			store := NewMemStorage()
			ProcessUrl(*domain, stream, store, "http://wwwdomaincom123doesnotexit")
			_, exists := store.Read("http://wwwdomaincom123doesnotexit")
			Ω(exists).Should(BeFalse())
		})

	})

})

func BenchmarkProcessUrl(b *testing.B) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "%s", pageWithThreeLinks)
	}))
	defer ts.Close()
	domain, _ := url.Parse("http:/wwwdomaincom123doesnotexit")
	for n := 0; n < b.N; n++ {
		stream := NewChannelStream()
		store := NewMemStorage()
		ProcessUrl(*domain, stream, store, ts.URL)
	}
}
