package crawl_test

import (
	. "bitbucket.org/grubersbucket/crawler/crawl"

	"fmt"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"sync/atomic"
	"time"
)

var _ = Describe("Crawl", func() {

	Context("Happy path", func() {

		It("should crawl a page with one link", func() {
			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				link := fmt.Sprintf("<a href=\"http://%s/test\">link</a>", r.Host)
				fmt.Fprintln(w, "<html><body>Page with one"+link+"</body></html>")
			}))
			defer ts.Close()

			dir, err := ioutil.TempDir("", "testoutput")
			if err != nil {
				Fail("can not create test output dir")
			}
			//defer os.RemoveAll(dir)
			fs, err := NewFileStorage(filepath.Join(dir, "output"))
			Ω(err).Should(BeNil())
			stream := NewChannelStream()
			err = Crawl(ts.URL, GoWorker, fs, stream)
			Ω(err).Should(BeNil())
			files, err := ioutil.ReadDir(filepath.Join(dir, "output"))
			Ω(err).Should(BeNil())
			fmt.Println(dir)
			Ω(len(files)).Should(BeNumerically("==", 2))
		})

		It("should finish when there are no more pages to process", func() {
			stream := NewChannelStream()
			var sitesToProcess int64
			WhenFinishedCloseChannel(stream, &sitesToProcess)
			_, ok := stream.Receive()
			Ω(ok).Should(BeFalse())
		})

		It("should block until there are no more pages to process", func() {
			stream := NewChannelStream()
			var sitesToProcess int64 = 1
			finished := make(chan bool)
			// gomega matcher?
			go func() {
				WhenFinishedCloseChannel(stream, &sitesToProcess)
				finished <- true
			}()
			select {
			case <-finished:
				Fail("WhenFinishedCloseChannel does not block")
			case <-time.Tick(time.Millisecond * 333):
			}
			atomic.AddInt64(&sitesToProcess, -1)
			_, ok := stream.Receive()
			Ω(ok).Should(BeFalse())
		})

	})

	Context("Error cases", func() {

		It("should not crawl when domain is wrong", func() {
			stream := NewChannelStream()
			s := NewMemStorage()
			err := Crawl("http://[::1]%23", GoWorker, s, stream)
			Ω(err).ShouldNot(BeNil())
		})

		It("should not crawl when url does not exist", func() {
			stream := NewChannelStream()
			s := NewMemStorage()
			err := Crawl("http://asdfasdfasdfsadf", GoWorker, s, stream)
			Ω(err).ShouldNot(BeNil())
		})

		It("should not crawl when there is no link in initial document", func() {
			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				fmt.Fprintln(w, "<html><body></body></html>")
			}))
			defer ts.Close()
			stream := NewChannelStream()
			s := NewMemStorage()
			err := Crawl(ts.URL, GoWorker, s, stream)
			Ω(err).Should(BeNil())
		})

		It("should not crawl when there is no link in initial document after normalization", func() {
			ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				fmt.Fprintln(w, "<html><body><a href=\"http://asfasdf/test\">link</a></body></html>")
			}))
			defer ts.Close()
			stream := NewChannelStream()
			s := NewMemStorage()
			err := Crawl(ts.URL, GoWorker, s, stream)
			Ω(err).Should(BeNil())
		})
	})

})
