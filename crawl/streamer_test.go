package crawl_test

import (
	. "bitbucket.org/grubersbucket/crawler/crawl"

	"fmt"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Streamer", func() {

	Describe("ChannelStream", func() {

		Context("Happy Path", func() {

			It("should be possible to send to and receive from channel", func() {
				cs := NewChannelStream()
				for i := 0; i < 100; i++ {
					cs.Send(fmt.Sprintf("%d", i))
				}
				for i := 0; i < 100; i++ {
					v, ok := cs.Receive()
					Ω(ok).Should(BeTrue())
					Ω(v).Should(Equal(fmt.Sprintf("%d", i)))
				}
				cs.Close()
				v, ok := cs.Receive()
				Ω(v).Should(Equal(""))
				Ω(ok).Should(BeFalse())
			})

		})

		Context("Errors", func() {

			It("should emit an error when sending failed (channel full)", func() {
				cs := NewChannelStream()
				for i := 0; i < 1024*128+2; i++ {
					cs.Send("")
				} // should not block when channel is full
			})

		})

	})

})
