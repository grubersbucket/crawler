package crawl_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestCrawl(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Crawl Suite")
}
