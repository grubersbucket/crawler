package crawl_test

import (
	. "bitbucket.org/grubersbucket/crawler/crawl"

	"fmt"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
)

var _ = Describe("Processing", func() {

	var goodDomains = []string{
		"https://www.google.eu",
		"http://www.gridengine.eu",
	}

	var pageWithZeroLinks = `<html><body></body></html>`
	var pageWithTwoLinks = `<html><body><a href="http://www.gmx.de">link1</a><a href="https://www.google.com">link2</a></body></html>`
	var pageWithRelativeLinks = `<html><body><a href="/link1">l1</a><a href="link2"><l2/a></body></html>`

	Describe("URL checking functions", func() {

		Context("Happy path", func() {

			It("should not error when having valid domains", func() {
				for _, domain := range goodDomains {
					_, err := CheckDomain(domain)
					Ω(err).Should(BeNil())
				}
			})

			It("should parse content for links", func() {
				links := ExtractLinks(strings.NewReader(pageWithTwoLinks))
				Ω(len(links)).Should(BeNumerically("==", 2))
				links = ExtractLinks(strings.NewReader(pageWithZeroLinks))
				Ω(len(links)).Should(BeNumerically("==", 0))
			})

			It("should parse content for relative links", func() {
				links := ExtractLinks(strings.NewReader(pageWithRelativeLinks))
				Ω(len(links)).Should(BeNumerically("==", 2))
			})

			It("should get and parse webpage for links", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					fmt.Fprintf(w, "%s", pageWithTwoLinks)
				}))
				defer ts.Close()
				links, err := DownloadPageAndParseForURLs(ts.URL)
				Ω(err).Should(BeNil())
				Ω(len(links)).Should(BeNumerically("==", 2))
			})

			It("should convert a relative URL to an absolute URL", func() {
				domain, _ := url.Parse("https://www.altavista.com")
				absolute, err := RelativeToAbsoluteURL(domain, "index.html")
				Ω(err).Should(BeNil())
				Ω(absolute.String()).Should(Equal("https://www.altavista.com/index.html"))
				absolute, err = RelativeToAbsoluteURL(domain, "/index.html")
				Ω(err).Should(BeNil())
				Ω(absolute.String()).Should(Equal("https://www.altavista.com/index.html"))
				absolute, err = RelativeToAbsoluteURL(domain, "http://www.altavista.com")
				Ω(err).Should(BeNil())
				Ω(absolute.String()).Should(Equal("http://www.altavista.com"))
			})

			It("should normalize all URLs", func() {
				domain, _ := url.Parse("https://www.paperball.com")
				urls := map[string]interface{}{
					"/index.html":         nil,
					"index.htm":           nil,
					"index.php":           nil,
					"/folder/../folder/":  nil,
					"/folder/../folder2/": nil,
					"folder/":             nil,
					"":                    nil,
				}
				n := NormalizeURLs(*domain, urls)
				Ω(len(n)).Should(BeNumerically("==", 5))
				Ω(n).Should(HaveKey("https://www.paperball.com/index.html"))
				Ω(n).Should(HaveKey("https://www.paperball.com/index.htm"))
				Ω(n).Should(HaveKey("https://www.paperball.com/index.php"))
				Ω(n).Should(HaveKey("https://www.paperball.com/folder/"))
				Ω(n).Should(HaveKey("https://www.paperball.com/folder2/"))
				Ω(n).ShouldNot(HaveKey("https://www.paperball.com/folder/../folder/"))
			})

			It("should filter URLs", func() {
				golang, _ := url.Parse("https://www.golang.org")
				urls := map[string]interface{}{
					"http://www.golang.org/index.html": nil,
					"https://www.ietf.org":             nil,
				}
				filterdURLs := FilterURLs(*golang, urls)
				Ω(len(filterdURLs)).Should(BeNumerically("==", 1))
			})

		})

		Context("Special cases", func() {

			var pageWithNoFollow = `<html><body><a rel="nofollow" href="https://www.domain.com/about/" title="About us" class="externalLink">about</a></body></html>`
			var pageWithMailTo = `<html><body><a href="mailto://www.domain.com">mail</a></body></html>`

			It("should parse content for links", func() {
				links := ExtractLinks(strings.NewReader(pageWithNoFollow))
				Ω(len(links)).Should(BeNumerically("==", 1))
			})

			It("should not return mailto as link", func() {
				links := ExtractLinks(strings.NewReader(pageWithMailTo))
				domain, _ := url.Parse("http://www.domain.com")
				Ω(len(NormalizeURLs(*domain, links))).Should(BeNumerically("==", 0))
			})

			It("should delete URL when it is not parsable during normalization", func() {
				golang, _ := url.Parse("https://www.golang.org")
				urls := map[string]interface{}{
					"index.html":      nil,
					"http://[::1]%23": nil,
				}
				n := NormalizeURLs(*golang, urls)
				Ω(len(n)).Should(BeNumerically("==", 1))
			})

			It("should fail HasValidDomain() check when hostnames are different", func() {
				google, _ := url.Parse("https://www.google.com")
				valid := HasValidDomain(google, "http://www.altavista.com")
				Ω(valid).Should(BeFalse())
			})

		})

		Context("Error cases", func() {

			It("should not have a valid domain when url is not processable", func() {
				altavista, _ := url.Parse("https://www.altavista.com")
				valid := HasValidDomain(altavista, "http://[::1]%23")
				Ω(valid).Should(BeFalse())
			})

			It("should allow only valid domains", func() {
				url, err := CheckDomain("http://[::1]%23")
				Ω(err).ShouldNot(BeNil())
				Ω(url).Should(BeNil())
			})

			It("should not download a non-existing side", func() {
				rc, err := Download("{}")
				Ω(err).ShouldNot(BeNil())
				Ω(rc).Should(BeNil())
			})

			It("should error when extracting links for non-existing url", func() {
				links, err := DownloadPageAndParseForURLs("{}")
				Ω(err).ShouldNot(BeNil())
				Ω(links).Should(BeNil())
			})

		})

		Describe("Benchmarks", func() {

			Context("Extracting links from the body", func() {

				Measure("ExtractLinks should be measured", func(b Benchmarker) {
					b.Time("ExtractLinks", func() {
						ExtractLinks(strings.NewReader(pageWithTwoLinks))
					})

				}, 100)

			})

			Context("Extracting links from the body", func() {

				Measure("ExtractLinks should be measured", func(b Benchmarker) {
					b.Time("ExtractLinks", func() {
						ExtractLinks(strings.NewReader(pageWithTwoLinks))
					})

				}, 100)

			})
			/*
				Context("Download the body of a page", func() {

					XMeasure("Download should be measured", func(b Benchmarker) {

						b.Time("Download", func() {
							body, err := Download("http://www.google.com")
							Ω(err).Should(BeNil())
							body.Close()
						})

					}, 1)

				}) */

		})

	})

})
