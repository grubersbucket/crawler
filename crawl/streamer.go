package crawl

import (
	"fmt"
	"os"
	"sync"
)

// CHANNEL_CAPACITY is the size of the buffer of the channels.
const CHANNEL_CAPACITY = 1024 * 128

// Streamer is an interface which abstracts capabilities required
// from a stream processing framework or unified log (like go channels,
// RabbitMQ, Kafka, ...)
type Streamer interface {
	// Send sends the string to the stream
	Send(string)
	// Receive returns a string from the stream or false if stream was closed
	Receive() (string, bool)
	// Closes a stream
	Close()
	// MaxWorkers return the amount of workers suggested to work on with the stream
	MaxWorkers() int
}

// ChannelStream implements a stream based on Go channels
type ChannelStream struct {
	sync.Mutex
	ch chan string
}

// NewChannelStream creates a new Go channel streamer
func NewChannelStream() Streamer {
	// 128k links should be enough for everyone :)
	return Streamer(&ChannelStream{ch: make(chan string, CHANNEL_CAPACITY)})
}

// Send writes a string in the channel.
func (cs *ChannelStream) Send(evt string) {
	select {
	case cs.ch <- evt:
	default:
		// page with a "warning" was not fully scanned
		// not expected -> channels should be big enough / prevent blocking workers
		// could be written to error stream, DB or other persistent storage for later processing
		fmt.Fprintf(os.Stderr, "warning: channel full: skipping: %s\n", evt)
	}
}

// Receive reads a string from the channel.
func (cs *ChannelStream) Receive() (string, bool) {
	result, ok := <-cs.ch
	return result, ok
}

// Close closes the channel.
func (cs *ChannelStream) Close() {
	close(cs.ch)
}

// MaxWorkers is the amount of workers which should subscribe to the channel.
// This could be moved out from the streamer.
func (cs *ChannelStream) MaxWorkers() int {
	return 96
}
