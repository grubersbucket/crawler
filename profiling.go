package main

import (
	"log"
	"os"
	"path/filepath"
	"runtime"
	"runtime/pprof"
	"runtime/trace"
)

func createProfilingFiles() (*os.File, *os.File, *os.File) {
	profileDirName := "profiles"
	if err := os.RemoveAll(profileDirName); err != nil {
		panic(err)
	}
	if err := os.Mkdir(profileDirName, 0700); err != nil {
		panic(err)
	}
	cpu, err := os.Create(filepath.Join(profileDirName, "cpuprofile"))
	if err != nil {
		panic(err)
	}
	trace, err := os.Create(filepath.Join(profileDirName, "trace"))
	if err != nil {
		panic(err)
	}
	mem, err := os.Create(filepath.Join(profileDirName, "memoryprofile"))
	if err != nil {
		panic(err)
	}
	return cpu, trace, mem
}

// Profile profiles the execution of a given function and stores the
// profiling files in the "profiles" subdirectory. If "profiles" exists
// all files are removed before!
func Profile(f func()) {
	cpuFile, traceFile, memFile := createProfilingFiles()
	defer cpuFile.Close()
	defer traceFile.Close()
	defer memFile.Close()

	if err := pprof.StartCPUProfile(cpuFile); err != nil {
		log.Fatalf("could not start CPU profiling: %s", err.Error())
	}
	defer pprof.StopCPUProfile()

	if err := trace.Start(traceFile); err != nil {
		log.Fatalf("failed to start trace: %s", err.Error())
	}
	defer trace.Stop()

	// do work
	f()

	runtime.GC() // get up-to-date statistics
	if err := pprof.WriteHeapProfile(memFile); err != nil {
		log.Fatalf("could not write memory profile: %s", err.Error())
	}
}
