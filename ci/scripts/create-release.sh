#!/usr/bin/env bash
set -e

git config --global user.email "crawler@danielgruber.com"
git config --global user.name "crawler-release-process"

version=`cat version/version`
gosrc="$GOPATH/src/bitbucket.org/grubersbucket"

echo "building version $version"

mkdir -p $gosrc
cp -r crawler $gosrc
pushd $gosrc/crawler
./xcompile.sh builds crawler $version 
popd

git clone crawler crawler-new-version
mkdir -p crawler-new-version/builds/current
pushd crawler-new-version/builds
cp -r $gosrc/crawler/builds/$version $version
cp $gosrc/crawler/builds/$version/* current/
git add $version/*
git add current/*
git commit -m "[ci skip] new version build $version"

