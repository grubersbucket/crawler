#!/usr/bin/env bash
set -e

go get github.com/onsi/ginkgo/ginkgo
go install github.com/onsi/ginkgo/ginkgo

mkdir -p $GOPATH/src/bitbucket.org/grubersbucket
cp -r crawler $GOPATH/src/bitbucket.org/grubersbucket

cd $GOPATH/src/bitbucket.org/grubersbucket/crawler/crawl

ginkgo

