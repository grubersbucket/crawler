#!/usr/bin/env bash
set -e

mkdir -p $GOPATH/src/bitbucket.org/grubersbucket
cp -r crawler $GOPATH/src/bitbucket.org/grubersbucket

cd $GOPATH/src/bitbucket.org/grubersbucket/crawler

./xcompile.sh builds crawler 0.0.0
