#!/usr/bin/env bash
set -e

go get -u github.com/alecthomas/gometalinter
go install github.com/alecthomas/gometalinter

gometalinter --install

mkdir -p $GOPATH/src/bitbucket.org/grubersbucket
cp -r crawler $GOPATH/src/bitbucket.org/grubersbucket

cd $GOPATH/src/bitbucket.org/grubersbucket/crawler

gometalinter --vendor --disable-all --enable=testify --enable=staticcheck --enable=deadcode --enable=gosimple --enable=unused --enable=goconst --enable=ineffassign --enable=test --enable=gofmt --enable=gocyclo --enable=misspell --enable=vetshadow --deadline=5m ./...